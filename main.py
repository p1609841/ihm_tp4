import csv
import random
from Student import Student
from scipy import stats


# Choose and returns a random student
def chooseStudent():
    numStudent = random.randint(0, 257)  # there are 258 students
    with open('userStats.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        line = 0
        for row in reader:
            if line == numStudent:
                student = Student(row['User'], row['achiever'], row['player'], row['socialiser'], row['freeSpirit'],
                                  row['disruptor'], row['philanthropist'], row['micoI'], row[' miacI'], row[' mistI'],
                                  row[' meinI'], row[' mereI'], row[' meidI'], row[' amotI'])
            line += 1
    return student


def printStudent(student):
    print(student.user)
    print('User Stats :')
    print('--------')
    print('achiever ' + str(student.achiever))
    print('player ' + str(student.player))
    print('socialiser ' + str(student.socialiser))
    print('freeSpirit ' + str(student.freespirit))
    print('disruptor ' + str(student.disruptor))
    print('mi ' + str(student.mi))
    print('me ' + str(student.me))
    print('amot ' + str(student.amot))
    print('--------')


# Analyses the impacts of each game element on the motivation (both intrinsic and extrinsic)
def analyseMotivationfunction():
    with open('userStats.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        GameElements = ['avatar', 'badges', 'progress', 'ranking', 'score',
                        'timer']
        analyse = {}
        for GE in GameElements:
            analyse[GE] = {
                'MI': 0,
                'ME': 0,
                'Amot': 0
            }
        for row in reader:  # we evaluate a game element function of the ratio CorrectCount / QuestionCount
            MI = int(row['micoI']) + int(row[' miacI']) + int(row[' mistI'])
            ME = int(row[' meinI']) + int(row[' mereI']) + int(row[' meidI'])
            Amot = int(row[' amotI'])
            for _, _ in analyse[row['GameElement']].items():
                ratio = int(row['CorrectCount']) / int(row['QuestionCount']) \
                        + int(
                    row['QuestionCount'])  # variation to reward the student when he answers to a lot of questions
                analyse[row['GameElement']]['MI'] += MI * ratio
                analyse[row['GameElement']]['ME'] += ME * ratio
                analyse[row['GameElement']]['Amot'] += Amot * ratio
    return analyse


# analyses the impact of each Hexad characteristic for each game element
def analyseHexadfunction():
    with open('userStats.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        GameElements = ['avatar', 'badges', 'progress', 'ranking', 'score',
                        'timer']
        analyse = {}
        for GE in GameElements:
            analyse[GE] = {
                'achiever': 0,
                'player': 0,
                'socialiser': 0,
                'freeSpirit': 0,
                'disruptor': 0,
                'philanthropist': 0
            }
        for row in reader:  # we evaluate a game element function of the ratio CorrectCount / QuestionCount
            for hexadAttribute, _ in analyse[row['GameElement']].items():
                ratio = int(row['CorrectCount']) / int(row['QuestionCount']) \
                        + int(
                    row['QuestionCount'])  # variation to reward the student when he answers to a lot of questions
                analyse[row['GameElement']][hexadAttribute] += int(row[hexadAttribute]) * ratio
    return analyse


def HexadBasedAdvice(student, complement):
    analyseHexad = {}
    if (complement):
        analyseHexad = analyseHexadfunction()
    else:
        GameElements = ['avatar', 'badges', 'progress', 'ranking', 'score',
                        'timer']
        for GE in GameElements:
            analyseHexad[GE] = {
                'achiever': 1,
                'player': 1,
                'socialiser': 1,
                'freeSpirit': 1,
                'disruptor': 1,
                'philanthropist': 1
            }
    with open('userStats.csv', newline='') as csvfile:
        # the final ranking container
        HexadBasedResults = {'avatar': 0, 'badges': 0, 'progress': 0, 'ranking': 0, 'score': 0,
                             'timer': 0}
        verifiedValues = {}
        coefMatrix = {
            'achiever': {},
            'player': {},
            'socialiser': {},
            'freeSpirit': {},
            'disruptor': {},
            'philanthropist': {}
        }
        pMatrix = {
            'achiever': {},
            'player': {},
            'socialiser': {},
            'freeSpirit': {},
            'disruptor': {},
            'philanthropist': {}
        }
        statScore = {}
        for GE, value in HexadBasedResults.items():
            statScore[GE] = {}
            pathCoef = 'R Code/PLS/Hexad/' + GE + 'PathCoefs.csv'
            pathP = 'R Code/PLS/Hexad/' + GE + 'pVals.csv'
            with open(pathCoef, newline='') as csvfileCoef:
                reader = csv.DictReader(csvfileCoef, delimiter=';')
                line = 0
                for row in reader:
                    if line == 0:
                        coefMatrix['achiever']['MI'] = row['achiever']
                        coefMatrix['player']['MI'] = row['player']
                        coefMatrix['socialiser']['MI'] = row['socialiser']
                        coefMatrix['freeSpirit']['MI'] = row['freeSpirit']
                        coefMatrix['disruptor']['MI'] = row['disruptor']
                        coefMatrix['philanthropist']['MI'] = row['philanthropist']
                    elif line == 1:
                        coefMatrix['achiever']['ME'] = row['achiever']
                        coefMatrix['player']['ME'] = row['player']
                        coefMatrix['socialiser']['ME'] = row['socialiser']
                        coefMatrix['freeSpirit']['ME'] = row['freeSpirit']
                        coefMatrix['disruptor']['ME'] = row['disruptor']
                        coefMatrix['philanthropist']['ME'] = row['philanthropist']
                    elif line == 2:
                        coefMatrix['achiever']['Amot'] = row['achiever']
                        coefMatrix['player']['Amot'] = row['player']
                        coefMatrix['socialiser']['Amot'] = row['socialiser']
                        coefMatrix['freeSpirit']['Amot'] = row['freeSpirit']
                        coefMatrix['disruptor']['Amot'] = row['disruptor']
                        coefMatrix['philanthropist']['Amot'] = row['philanthropist']
                    else:
                        print("error in number of lines in the coef-matrix")
                    line += 1

            with open(pathP, newline='') as csvfileP:
                reader = csv.DictReader(csvfileP, delimiter=';')
                line = 0
                for row in reader:
                    if line == 0:
                        pMatrix['achiever']['MI'] = row['achiever']
                        pMatrix['player']['MI'] = row['player']
                        pMatrix['socialiser']['MI'] = row['socialiser']
                        pMatrix['freeSpirit']['MI'] = row['freeSpirit']
                        pMatrix['disruptor']['MI'] = row['disruptor']
                        pMatrix['philanthropist']['MI'] = row['philanthropist']
                    elif line == 1:
                        pMatrix['achiever']['ME'] = row['achiever']
                        pMatrix['player']['ME'] = row['player']
                        pMatrix['socialiser']['ME'] = row['socialiser']
                        pMatrix['freeSpirit']['ME'] = row['freeSpirit']
                        pMatrix['disruptor']['ME'] = row['disruptor']
                        pMatrix['philanthropist']['ME'] = row['philanthropist']
                    elif line == 2:
                        pMatrix['achiever']['Amot'] = row['achiever']
                        pMatrix['player']['Amot'] = row['player']
                        pMatrix['socialiser']['Amot'] = row['socialiser']
                        pMatrix['freeSpirit']['Amot'] = row['freeSpirit']
                        pMatrix['disruptor']['Amot'] = row['disruptor']
                        pMatrix['philanthropist']['Amot'] = row['philanthropist']
                    else:
                        print("error in number of lines in the p-matrix")
                    line += 1

            verifiedValues[GE] = {}
            for stat, content in coefMatrix.items():
                # GE = Game Element
                # stat = Hexad Profile Statistic (like : "achiever")
                # content = { 'MI' : x, 'ME' : y, 'Amot' : z }

                verifiedValues[GE][stat] = {}
                for mot in ['MI', 'ME', 'Amot']:
                    if (float(pMatrix[stat][mot])) < acceptablePvalue:
                        verifiedValues[GE][stat][mot] = float(coefMatrix[stat][mot])
                    else:
                        verifiedValues[GE][stat][mot] = 0

                statScore[GE][stat] = float(content['MI']) + float(content['ME']) - float(content['Amot'])

            HexadBasedResults[GE] = student.achiever * statScore[GE]['achiever'] * analyseHexad[GE]['achiever'] + \
                                    student.player * statScore[GE]['player'] * analyseHexad[GE]['player'] + \
                                    student.socialiser * statScore[GE]['socialiser'] * analyseHexad[GE][
                                        'socialiser'] + \
                                    student.freespirit * statScore[GE]['freeSpirit'] * analyseHexad[GE][
                                        'freeSpirit'] + \
                                    student.disruptor * statScore[GE]['disruptor'] * analyseHexad[GE]['disruptor'] + \
                                    student.philantropist * statScore[GE]['philanthropist'] * analyseHexad[GE][
                                        'philanthropist']
    result = sorted(HexadBasedResults.items(), key=lambda x: x[1], reverse=True)
    return result


def MotivationBasedAdvice(student, complement):
    analyseMotivation = {}
    if (complement):
        analyseMotivation = analyseMotivationfunction()
    else:
        GameElements = ['avatar', 'badges', 'progress', 'ranking', 'score',
                        'timer']
        for GE in GameElements:
            analyseMotivation[GE] = {
                'MI': 1,
                'ME': 1,
                'Amot': 1
            }
    with open('userStats.csv', newline='') as csvfile:
        # the final ranking container
        MotivationBasedResults = {'avatar': 0, 'badges': 0, 'progress': 0, 'ranking': 0, 'score': 0,
                                  'timer': 0}
        verifiedValues = {}
        coefMatrix = {
            'MIuser': {},
            'MEuser': {},
            'Amotuser': {}
        }
        pMatrix = {
            'MIuser': {},
            'MEuser': {},
            'Amotuser': {}
        }
        statScore = {}
        for GE, value in MotivationBasedResults.items():
            statScore[GE] = {}
            pathCoef = 'R Code/PLS/Motivation/' + GE + 'PathCoefs.csv'
            pathP = 'R Code/PLS/Motivation/' + GE + 'pVals.csv'
            with open(pathCoef, newline='') as csvfileCoef:
                reader = csv.DictReader(csvfileCoef, delimiter=';')
                line = 0
                for row in reader:
                    if line == 0:
                        coefMatrix['MIuser']['MI'] = row['MI']
                        coefMatrix['MEuser']['MI'] = row['ME']
                        coefMatrix['Amotuser']['MI'] = row['amotI']
                    elif line == 1:
                        coefMatrix['MIuser']['ME'] = row['MI']
                        coefMatrix['MEuser']['ME'] = row['ME']
                        coefMatrix['Amotuser']['ME'] = row['amotI']
                    elif line == 2:
                        coefMatrix['MIuser']['Amot'] = row['MI']
                        coefMatrix['MEuser']['Amot'] = row['ME']
                        coefMatrix['Amotuser']['Amot'] = row['amotI']
                    else:
                        print("error in number of lines in the coef-matrix")
                    line += 1

            with open(pathP, newline='') as csvfileP:
                reader = csv.DictReader(csvfileP, delimiter=';')
                line = 0
                for row in reader:
                    if line == 0:
                        pMatrix['MIuser']['MI'] = row['MI']
                        pMatrix['MEuser']['MI'] = row['ME']
                        pMatrix['Amotuser']['MI'] = row['amotI']
                    elif line == 1:
                        pMatrix['MIuser']['ME'] = row['MI']
                        pMatrix['MEuser']['ME'] = row['ME']
                        pMatrix['Amotuser']['ME'] = row['amotI']
                    elif line == 2:
                        pMatrix['MIuser']['Amot'] = row['MI']
                        pMatrix['MEuser']['Amot'] = row['ME']
                        pMatrix['Amotuser']['Amot'] = row['amotI']
                    else:
                        print("error in number of lines in the p-matrix")
                    line += 1

            verifiedValues[GE] = {}
            for stat, content in coefMatrix.items():
                # GE = Game Element
                # stat = Motivation Profile Statistic (like : "MI")
                # content = { 'MI' : x, 'ME' : y, 'Amot' : z }

                verifiedValues[GE][stat] = {}
                for mot in ['MI', 'ME', 'Amot']:
                    if (float(pMatrix[stat][mot])) < acceptablePvalue:
                        verifiedValues[GE][stat][mot] = float(coefMatrix[stat][mot])
                    else:
                        verifiedValues[GE][stat][mot] = 0

                statScore[GE][stat] = float(content['MI']) + float(content['ME']) - float(content['Amot'])

            MotivationBasedResults[GE] = student.mi * statScore[GE]['MIuser'] * analyseMotivation[GE]['MI'] + \
                                         student.me * statScore[GE]['MEuser'] * analyseMotivation[GE]['ME'] + \
                                         student.amot * statScore[GE]['Amotuser'] * analyseMotivation[GE]['Amot'] \
                                         * 3  # because the other statistics are a sum of 3 elements
    result = sorted(MotivationBasedResults.items(), key=lambda x: x[1], reverse=True)
    return result


def MixedAdvice(hexad, motivation):
    # print(hexad)  # print the results of the previous analysis (hexad)
    # print(motivation)  # print the results of the previous analysis (motivation)
    rank = 1
    newAffinityVector = {}
    for elt in hexad:
        GE = elt[0]
        if (float(elt[1]) > 0):
            newAffinityVector[GE] = 6 - rank
        else:
            newAffinityVector[GE] = (6 - rank) / 10
        rank += 1

    rank = 1
    for elt in motivation:
        GE = elt[0]
        if (float(elt[1]) > 0):
            newAffinityVector[GE] = 6 - rank
        else:
            newAffinityVector[GE] = (6 - rank) / 10
        rank += 1
    result = sorted(newAffinityVector.items(), key=lambda x: x[1], reverse=True)
    return result


def statistics(complement):
    questionCountarrayHexadwithGE = []
    questionCountarrayHexadwithoutGE = []
    questionCountarrayMotivationwithGE = []
    questionCountarrayMotivationwithoutGE = []
    ratiotarrayHexadwithGE = []
    ratiotarrayHexadwithoutGE = []
    ratiotarrayMotivationwithGE = []
    ratiotarrayMotivationwithoutGE = []
    timearrayHexadwithGE = []
    timearrayHexadwithoutGE = []
    timearrayMotivationwithGE = []
    timearrayMotivationwithoutGE = []
    questionCountarrayMixedwithGE = []
    questionCountarrayMixedwithoutGE = []
    ratiotarrayMixedwithGE = []
    ratiotarrayMixedwithoutGE = []
    timearrayMixedwithGE = []
    timearrayMixedwithoutGE = []
    motivationvariationarrayHexadwithGE = []
    motivationvariationarrayHexadwithoutGE = []
    motivationvariationarrayMotivationwithGE = []
    motivationvariationarrayMotivationwithoutGE = []
    motivationvariationarrayMixedwithGE = []
    motivationvariationarrayMixedwithoutGE = []

    with open('userStats.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            student = Student(row['User'], row['achiever'], row['player'], row['socialiser'], row['freeSpirit'],
                              row['disruptor'], row['philanthropist'], row['micoI'], row[' miacI'], row[' mistI'],
                              row[' meinI'], row[' mereI'], row[' meidI'], row[' amotI'])
            HexadVector = HexadBasedAdvice(student, complement)
            HexadGE = HexadVector[0][0]
            MotivationVector = MotivationBasedAdvice(student, complement)
            MotivationGE = MotivationVector[0][0]
            MixedGE = MixedAdvice(HexadVector, MotivationVector)[0][0]

            # If we based our statistics on the amount of Questions answered by the student :

            if (HexadGE == row['GameElement']):
                questionCountarrayHexadwithGE.append(int(row['QuestionCount']))
            else:
                questionCountarrayHexadwithoutGE.append(int(row['QuestionCount']))
            if (MotivationGE == row['GameElement']):
                questionCountarrayMotivationwithGE.append(int(row['QuestionCount']))
            else:
                questionCountarrayMotivationwithoutGE.append(int(row['QuestionCount']))
            if (MixedGE == row['GameElement']):
                questionCountarrayMixedwithGE.append(int(row['QuestionCount']))
            else:
                questionCountarrayMixedwithoutGE.append(int(row['QuestionCount']))

            # If we based our statistics on the ratio of (Corrected Answers / Questions answered) :

            if (HexadGE == row['GameElement']):
                ratiotarrayHexadwithGE.append(int(row['QuestionCount']) / int(row['CorrectCount']))
            else:
                ratiotarrayHexadwithoutGE.append(int(row['QuestionCount']) / int(row['CorrectCount']))
            if (MotivationGE == row['GameElement']):
                ratiotarrayMotivationwithGE.append(int(row['QuestionCount']) / int(row['CorrectCount']))
            else:
                ratiotarrayMotivationwithoutGE.append(int(row['QuestionCount']) / int(row['CorrectCount']))
            if (MixedGE == row['GameElement']):
                ratiotarrayMixedwithGE.append(int(row['QuestionCount']) / int(row['CorrectCount']))
            else:
                ratiotarrayMixedwithoutGE.append(int(row['QuestionCount']) / int(row['CorrectCount']))

            # If we based our statistics on the amount of time spent by the student :
            timestr = row['Time']

            ftr = [3600, 60, 1]

            time = sum([a * b for a, b in zip(ftr, map(int, timestr.split(':')))])
            if (HexadGE == row['GameElement']):
                timearrayHexadwithGE.append(time)
            else:
                timearrayHexadwithoutGE.append(time)
            if (MotivationGE == row['GameElement']):
                timearrayMotivationwithGE.append(time)
            else:
                timearrayMotivationwithoutGE.append(time)
            if (MixedGE == row['GameElement']):
                timearrayMixedwithGE.append(time)
            else:
                timearrayMixedwithoutGE.append(time)

            # If we based our statistics on the motivation variation :
            # Var = micoVar + miacVar + mistVar + meidVar + meinVar + mereVar + amotVar
            Var = int(row[' micoVar']) + int(row[' miacVar']) + int(row[' mistVar']) + int(row[' meidVar']) + int(row[' meinVar']) + int(row[' mereVar']) + int(row[' amotVar'])
            if (HexadGE == row['GameElement']):
                motivationvariationarrayHexadwithGE.append(Var)
            else:
                motivationvariationarrayHexadwithoutGE.append(Var)
            if (MotivationGE == row['GameElement']):
                motivationvariationarrayMotivationwithGE.append(Var)
            else:
                motivationvariationarrayMotivationwithoutGE.append(Var)
            if (MixedGE == row['GameElement']):
                motivationvariationarrayMixedwithGE.append(Var)
            else:
                motivationvariationarrayMixedwithoutGE.append(Var)
    # Hexad Statistics
    print("Hexad advice Statistics : ")
    print("")
    print("Based on amount of questions answered :")
    t, p = stats.ttest_ind(questionCountarrayHexadwithGE, questionCountarrayHexadwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on the ratio of corrected answered by questions answered :")
    t, p = stats.ttest_ind(ratiotarrayHexadwithGE, ratiotarrayHexadwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on amount of time spent on the exercise :")
    t, p = stats.ttest_ind(timearrayHexadwithGE, timearrayHexadwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on the motivation variation :")
    t, p = stats.ttest_ind(motivationvariationarrayHexadwithGE, motivationvariationarrayHexadwithoutGE, axis=0,
                           equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("---")
    print("")

    # Motivation Statistics
    print("Motivation advice Statistics : ")
    print("")
    print("Based on amount of questions answered :")
    t, p = stats.ttest_ind(questionCountarrayMotivationwithGE, questionCountarrayMotivationwithoutGE, axis=0,
                           equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on the ratio of corrected answered by questions answered :")
    t, p = stats.ttest_ind(ratiotarrayMotivationwithGE, ratiotarrayMotivationwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on amount of time spent on the exercise :")
    t, p = stats.ttest_ind(timearrayMotivationwithGE, timearrayMotivationwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on the motivation variation :")
    t, p = stats.ttest_ind(motivationvariationarrayMotivationwithGE, motivationvariationarrayMotivationwithoutGE,
                           axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("---")

    # Mixed Statistics
    print("Mixed advice Statistics : ")
    print("")
    print("Based on amount of questions answered :")
    t, p = stats.ttest_ind(questionCountarrayMixedwithGE, questionCountarrayMixedwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on the ratio of corrected answered by questions answered :")
    t, p = stats.ttest_ind(ratiotarrayMixedwithGE, ratiotarrayMixedwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on amount of time spent on the exercise :")
    t, p = stats.ttest_ind(timearrayMixedwithGE, timearrayMixedwithoutGE, axis=0, equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("Based on the motivation variation :")
    t, p = stats.ttest_ind(motivationvariationarrayMixedwithGE, motivationvariationarrayMixedwithoutGE, axis=0,
                           equal_var=False)
    print("t = " + str(t))
    print("p = " + str(p))
    print("---")


def main(complement):
    # Main code for Step 3
    student = chooseStudent()
    printStudent(student)
    # Hexad Advice
    print(
        'Here are the suggested Game Elements for this student (based on his Hexad Profile , from better to worse) : ')
    Hexad = HexadBasedAdvice(student, complement)
    # print(Hexad)
    i = 1
    for key, value in Hexad:
        print(str(i) + ')- ' + key)
        i += 1
    print('-------------')
    print(
        'Here are the suggested Game Elements for this student (based on his Motivation Profile , from better to worse) : ')
    Motivation = MotivationBasedAdvice(student, complement)
    # print(Motivation)
    i = 1
    for key, value in Motivation:
        print(str(i) + ')- ' + key)
        i += 1

    # Main code for Part 2
    print('-------------')
    Mixed = MixedAdvice(Hexad, Motivation)
    print(
        'Here are the suggested Game Elements for this student (based on both his Motivation Profile and Hexad Profile , from better to worse) : ')
    i = 1
    for key, value in Mixed:
        print(str(i) + ')- ' + key)
        i += 1
    print('-------------')
    statistics(complement)


acceptablePvalue = 0.05  # What amount of Pvalue is necessary to accept the result of the Coef Matrix
complement = False  # Do you want to use the ratio of good answers / total answers to create the affinity vector
main(complement)
