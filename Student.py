# a student is defined by this class
class Student:
    def __init__(self, User, Achiever, Player, Socialiser, Freespirit, Disruptor, Philantropist, micoI, miacI, mistI,
                 meinI, mereI, meidI,amotI):
        self.user = User  # ID
        # Hexad statistics
        self.achiever = int(Achiever)
        self.player = int(Player)
        self.socialiser = int(Socialiser)
        self.freespirit = int(Freespirit)
        self.disruptor = int(Disruptor)
        self.philantropist = int(Philantropist)
        # motivation statistics
        self.mi = int(miacI) + int(micoI) + int(mistI)  # Intrinsic motivation
        self.me = int(meidI) + int(mereI) + int(meinI)  # Extrinsic motivation
        self.amot = int(amotI)

    # returns the most important statistic of the hexad profile and his amount
    def hexadProfile(self):
        maxAttribute = max(self.achiever, self.player, self.socialiser, self.freespirit, self.philantropist)
        if maxAttribute == self.achiever:
            return ['achiever', self.achiever]
        elif maxAttribute == self.player:
            return ['player', self.player]
        elif maxAttribute == self.socialiser:
            return ['socialiser', self.socialiser]
        elif maxAttribute == self.freespirit:
            return ['freeSpirit', self.freespirit]
        elif maxAttribute == self.philantropist:
            return ['philanthropist', self.philantropist]
        else:
            return ['disruptor', self.disruptor]
